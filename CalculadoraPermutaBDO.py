import os

def ConstroiTier(bRequerida):
    dicionarioTier["T1"] = bRequerida*6
    dicionarioTier["T2"] = bRequerida*2
    dicionarioTier["T3"] = bRequerida*4
    dicionarioTier["T4"] = bRequerida*6
    dicionarioTier["T5"] = bRequerida*4


def printTrocasSimples(barganha):
    print()
    print("Trocas T4->T5: ", (barganha//dicionarioTier["T5"]), " Sobrando ", (barganha%(bRequerida*4)), " de barganha.")
    print("Trocas T3->T4: ", (barganha//dicionarioTier["T4"]), " Sobrando ", (barganha%(bRequerida*6)), " de barganha.")
    print("Trocas T2->T3: ", (barganha//dicionarioTier["T3"]), " Sobrando ", (barganha%(bRequerida*4)), " de barganha.")
    print("Trocas T1->T2: ", (barganha//dicionarioTier["T2"]), " Sobrando ", (barganha%(bRequerida*2)), " de barganha.")
    print("Trocas T0->T1: ", (barganha//dicionarioTier["T1"]), " Sobrando ", (barganha%(bRequerida*6)), " de barganha.")
    print()
    

def printTrocasEspecifivas(barganha, parametros):
    for item in parametros:
        arrP = item.upper().split("T")
        barganha -= (int(arrP[0]) * dicionarioTier["T{}".format(arrP[1])])
    
    if barganha >= 0:
        print("A sequencia de trocas é possível, sobrando {} de barganha.".format(barganha))
    else:
        print("A sequencia de trocas não é possivel, faltou {} de barganha para realiza-la.".format(barganha*-1))


try:
    parametros = []
    dicionarioTier = {}

    print("Digite a barganha requerida.")
    bRequerida =  int(input())
    ConstroiTier(bRequerida)


    while True:
        print("Digite a barganha restante")
        parametros = input().split(" ")
        barganha = int(parametros[0])
        parametros.pop(0)

        if len(parametros) == 0:
            printTrocasSimples(barganha)
        else:
            printTrocasEspecifivas(barganha, parametros)

        
except KeyboardInterrupt:
    os._exit(0)
except Exception as e:
    print("ERRO: ", e,"\nFinalizando programa...")
    os._exit(0) 